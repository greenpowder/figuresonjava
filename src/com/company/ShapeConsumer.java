package com.company;

import java.util.Random;

import com.figures.DeleteShapeSubscriber;
import com.figures.HasWeight;
import com.figures.Shape;
import com.figures.Space;


class ShapeConsumer extends Thread implements DeleteShapeSubscriber {
    private final int demand;
    private final Space space;

    public ShapeConsumer(String name, Space space, int demand) {
        super(name);
        this.demand = demand;
        this.space = space;
        this.start();
    }

    @Override
    public void run() {
        Random rand = new Random();

        for (int i = 0; i < demand; ++i)
        {
            synchronized (space) {
                try {
                    while (space.isEmpty()) {
                        space.notifyAll();
                        space.wait();
                    }

                    HasWeight shape = (HasWeight)space.getShapes().get(rand.nextInt(space.len()));

                    space.delete((Shape)shape);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                Thread.sleep(100 * rand.nextInt(10));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
