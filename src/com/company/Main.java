package com.company;

import com.figures.*;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Создаём пространство
        Space sp = Space.getInstance();
        sp.setMaxTotalWeight(20);

        // Запускаем многопоточную обработку
        synchronized (sp) {
            StateInformator si = new StateInformator(10);

            ShapeProducer shp = new ShapeProducer("Producer1", sp, 100);
            ShapeConsumer shc = new ShapeConsumer("Consumer", sp, 100);

            sp.setAddShapeSubscribers(List.of(shp, si));
            sp.setDeleteShapeSubscribers(List.of(shc, si));
        }
    }
}
