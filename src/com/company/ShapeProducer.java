package com.company;

import com.figures.*;

import java.util.Random;

class ShapeProducer extends Thread implements AddShapeSubscriber {
    private final int pull;
    private final Space space;

    public ShapeProducer(String name, Space space, int pull) {
        super(name);
        this.pull = pull;
        this.space = space;
        this.start();
    }

    @Override
    public void run() {
        Random rand = new Random();

        for (int i = 0; i < pull; ++i)
        {
            synchronized (space) {
                try {
                    HasWeight shape;
                    switch (rand.nextInt(5)) {
                        default:
                        case 0:
                            shape = new Cube(rand.nextInt(101));
                            break;
                        case 1:
                            shape = new Cylinder(rand.nextInt(101), rand.nextInt(101));
                            break;
                        case 2:
                            shape = new Sphere(rand.nextInt(101));
                            break;
                        case 3:
                            shape = new SquarePyramid(rand.nextInt(101), rand.nextInt(101));
                            break;
                        case 4:
                            shape = new TriangularPyramid(rand.nextInt(101), rand.nextInt(101));
                            break;
                    }

                    shape.setWeight((double) rand.nextInt(100 * (int)space.getMaxTotalWeight() + 1) / 101);

                    while (space.getTotalWeight() + shape.getWeight() > space.getMaxTotalWeight()) {
                        space.notifyAll();
                        space.wait();
                    }

                    space.add((Shape) shape);
                    space.notifyAll();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try {
                Thread.sleep(100 * rand.nextInt(8));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
