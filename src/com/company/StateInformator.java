package com.company;

import com.figures.AddShapeSubscriber;
import com.figures.DeleteShapeSubscriber;
import com.figures.Shape;
import com.figures.Space;

public class StateInformator implements AddShapeSubscriber, DeleteShapeSubscriber {
    private final Space target = Space.getInstance();

    private final int max_counter;
    private int counter = 1;

    public StateInformator(int max_counter)
    {
        this.max_counter = max_counter;
    }

    @Override
    public void onSubscribe(Shape shape) {
        if (counter == max_counter) {
            System.out.println("=====\nНа данный момент в Space находится " + target.len() + " фигур\nОбщая масса - " + String.format("%.2f", Space.getInstance().getTotalWeight()) + "\n=====");
            counter = 1;
        } else {
            ++counter;
        }
    }
}
