package com.figures;

import com.figures.HasWeight;
import com.figures.Shape;

public interface DeleteShapeSubscriber {
    default void onSubscribe(Shape shape) {
        System.out.println("Была удалена фигура " + shape.getClass().getSimpleName() + ", массой " + String.format("%.2f", ((HasWeight)shape).getWeight()));
    }
}
