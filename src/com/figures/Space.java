package com.figures;

import java.io.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Space implements Serializable {
    private static final Space instance = new Space();

    public static Space getInstance() {
        return instance;
    }

    List<AddShapeSubscriber> addShapeSubscribers = new ArrayList<AddShapeSubscriber>(
            Collections.singleton(new AddShapeSubscriber() {
                @Override
                public void onSubscribe(Shape shape) {
                }
            })
            );

    List<DeleteShapeSubscriber> deleteShapeSubscribers = new ArrayList<DeleteShapeSubscriber>(
            Collections.singleton(new DeleteShapeSubscriber() {
                @Override
                public void onSubscribe(Shape shape) {
                }
            })
    );

    public void setAddShapeSubscribers(List<AddShapeSubscriber> addShapeSubscribers) {
        this.addShapeSubscribers = addShapeSubscribers;
    }

    public void setDeleteShapeSubscribers(List<DeleteShapeSubscriber> deleteShapeSubscribers) {
        this.deleteShapeSubscribers = deleteShapeSubscribers;
    }

    private final ArrayList<Shape> shapes = new ArrayList<>();
    private double maxTotalWeight = 0;

    private Space() {}

    private Space(String path) {
        addLoad(path);
    }

    public void save(String path) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))){
            oos.writeObject(shapes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addLoad(String path) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))) {
            shapes.addAll((ArrayList<Shape>) ois.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Shape> getShapes() {
        return shapes;
    }

    public boolean isEmpty() {
        return getShapes().isEmpty();
    }

    public int len() {
        return getShapes().size();
    }

    public void add(Collection<Shape> shape_collection) {
        if (getTotalWeight() < getMaxTotalWeight())
        {
            shapes.addAll(shape_collection);
            addShapeSubscribers.forEach(
                    a -> shape_collection.forEach(a::onSubscribe)
            );
        }
    }

    public void add(Shape shape) {
        getShapes().add(shape);
        addShapeSubscribers.forEach(a -> a.onSubscribe(shape));
    }

    public void delete(Collection<Shape> shapes) {
        getShapes().removeAll(shapes);
        deleteShapeSubscribers.forEach(
                a -> shapes.forEach(a::onSubscribe)
        );
    }

    public void delete(Shape shape) {
        getShapes().remove(shape);
        deleteShapeSubscribers.forEach(a -> a.onSubscribe(shape));
    }

    public void delete(int index) {
        deleteShapeSubscribers.forEach(
                a -> a.onSubscribe(getShapes().remove(index))
        );
    }

    public double getMaxTotalWeight() {
        return maxTotalWeight;
    }

    public void setMaxTotalWeight(double weight) {
        this.maxTotalWeight = weight;
    }

    public double getTotalWeight() {
        return getShapes().stream()
                .filter(a -> a instanceof HasWeight)
                .map(a -> (HasWeight)a)
                .mapToDouble(HasWeight::getWeight)
                .sum();
    }

    public double getTotalVolume() {
        return getShapes().stream()
                .mapToDouble(Shape::getVolume)
                .sum();
    }

    public double getTotalSurfaceSquare() {
        return getShapes().stream()
                .mapToDouble(Shape::getSurfaceSquare)
                .sum();
    }

    public void scaleShapes(double num) {
        getShapes().stream()
                .filter(a -> a instanceof Scalable)
                .forEach(a -> ((Scalable) a).scale(num));
    }

    public List<Shape> filterBy(Predicate<Shape> shapePredicate) {
        return getShapes().stream()
                .filter(shapePredicate)
                .collect(Collectors.toList()
                );
    }

    public List<Shape> sortBy(Comparator<Shape> shapeComparator) {
        return getShapes().stream()
                .sorted(shapeComparator)
                .collect(Collectors.toList()
                );
    }
}
