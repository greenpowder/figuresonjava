package com.figures;

public interface Scalable {
    void scale(double num);
}
