package com.figures;

import com.figures.HasWeight;
import com.figures.Shape;

public interface AddShapeSubscriber {
    default void onSubscribe(Shape shape) {
        System.out.println("Была добавлена фигура " + shape.getClass().getSimpleName() + ", массой " + String.format("%.2f", ((HasWeight)shape).getWeight()));
    }
}
