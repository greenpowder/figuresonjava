package com.figures;

import java.io.Serializable;

abstract public class Shape implements Serializable {
    abstract public double getSurfaceSquare();
    abstract public double getVolume();
}
