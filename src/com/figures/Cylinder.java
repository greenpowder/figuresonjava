package com.figures;

public class Cylinder extends SolidOfRevolution implements HasWeight, Scalable {
    private double weight = 0;
    private double height;

    public Cylinder(double height, double radius) {
        setHeight(height);
        setRadius(radius);
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getSurfaceSquare() {
        return 2 * Math.PI * (Math.pow(getRadius(), 2) + getRadius() * height);
    }

    @Override
    public double getVolume() {
        return Math.pow(getRadius(), 2) * Math.PI * height;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public double getDensity() {
        return getWeight() / getVolume();
    }

    @Override
    public void scale(double num) {
        this.weight *= num;
        setRadius(getRadius() * Math.pow(num, 1/3.0));
        this.height *= Math.pow(num, 1/3.0);
    }
}
