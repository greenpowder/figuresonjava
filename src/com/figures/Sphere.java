package com.figures;

public class Sphere extends SolidOfRevolution implements HasWeight, Scalable {
    private double weight = 0;

    public Sphere(double radius) {
        setRadius(radius);
    }

    @Override
    public double getSurfaceSquare() {
        return 4 * Math.pow(getRadius(), 2) * Math.PI;
    }

    @Override
    public double getVolume() {
        return 4 / 3.0 * Math.pow(getRadius(), 3) * Math.PI;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public double getDensity() {
        return getWeight() / getWeight();
    }

    @Override
    public void scale(double num) {
        this.weight *= num;
        setRadius(getRadius() * Math.pow(num, 1/3.0));
    }
}
