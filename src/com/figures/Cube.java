package com.figures;

public class Cube extends Shape implements HasWeight, Scalable {
    private double weight = 0;
    private double edgeLength;

    public Cube(double edgeLength)
    {
        setEdgeLength(edgeLength);
    }

    public double getEdgeLength() {
        return edgeLength;
    }

    public void setEdgeLength(double edgeLength) {
        this.edgeLength = edgeLength;
    }

    @Override
    public double getSurfaceSquare() {
        return Math.pow(edgeLength, 2) * 6;
    }

    @Override
    public double getVolume() {
        return Math.pow(edgeLength, 3);
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public double getDensity() {
        return getWeight() / getVolume();
    }

    @Override
    public void scale(double num) {
        this.weight *= num;
        this.edgeLength *= Math.pow(num, 1/3.0);
    }
}
