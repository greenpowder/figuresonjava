package com.figures;

public interface HasWeight {
    public double getWeight();
    public void setWeight(double weight);
    public double getDensity();
}
