package com.figures;

public class SquarePyramid extends Shape implements HasWeight, Scalable {
    private double weight = 0;
    private double edgeLength;
    private double height;

    public SquarePyramid(double height, double edgeLength) {
        setEdgeLength(edgeLength);
        setHeight(height);
    }

    public double getEdgeLength() {
        return edgeLength;
    }

    public void setEdgeLength(double edgeLength) {
        this.edgeLength = edgeLength;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getSurfaceSquare() {
        return Math.pow(edgeLength, 2) + 4 * edgeLength * Math.sqrt(Math.pow(edgeLength / 2, 2) + Math.pow(height, 2));
    }

    @Override
    public double getVolume() {
        return 1/3.0 * Math.pow(edgeLength, 2) * height;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public double getDensity() {
        return getWeight() / getVolume();
    }

    @Override
    public void scale(double num) {
        this.weight *= num;
        this.edgeLength *= Math.pow(num, 1/3.0);
        this.height *= Math.pow(num, 1/3.0);
    }
}
