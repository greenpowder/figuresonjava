package com.figures;

public class TriangularPyramid extends Shape implements HasWeight, Scalable {
    private double weight = 0;
    private double height;
    private double edgeLength;

    public TriangularPyramid(double height, double edgeLength) {
        setEdgeLength(edgeLength);
        setHeight(height);
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getEdgeLength() {
        return edgeLength;
    }

    public void setEdgeLength(double edgeLength) {
        this.edgeLength = edgeLength;
    }

    @Override
    public double getSurfaceSquare() {
        double s_main = (Math.sqrt(3) / 4.0) * Math.pow(edgeLength, 2);
        double s_side = 0.5 * edgeLength * Math.sqrt(Math.pow(height, 2) + Math.pow(edgeLength, 2) / 4.0);
        return s_main + 3 * s_side;
    }

    @Override
    public double getVolume() {
        return height * Math.pow(edgeLength, 2) / (4 * Math.sqrt(3));
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public double getDensity() {
        return getWeight() / getVolume();
    }

    @Override
    public void scale(double num) {
        this.weight *= num;
        this.edgeLength *= Math.pow(num, 1/3.0);
        this.height *= Math.pow(num, 1/3.0);
    }
}
